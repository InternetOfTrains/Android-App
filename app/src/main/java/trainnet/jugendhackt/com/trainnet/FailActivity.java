package trainnet.jugendhackt.com.trainnet;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class FailActivity extends AppCompatActivity {

    ConnectivityManager connManager;
    NetworkInfo wifiConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fail);

        //Initialize elements
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    //Function to Retry to connect to the Internet
    public void retry(View view){

        //Check if Wifi is Connected
        wifiConnection  = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if(wifiConnection.isConnected()){

            //Direct the user to the Start-Page
            Intent isConnectedIntent = new Intent(view.getContext(), ResultActivity.class);
            startActivityForResult(isConnectedIntent, 0);

        }else if(!wifiConnection.isConnected()){

            //Direct the user to the fail-page
            Intent isConnectedIntent = new Intent(view.getContext(), FailActivity.class);
            startActivityForResult(isConnectedIntent, 0);

        }

    }
}
