package trainnet.jugendhackt.com.trainnet;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

public class ResultActivity extends AppCompatActivity {

    ServerConnection serverConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //Initialize Items
        serverConnection = new ServerConnection();

        //Create connection to JSON
        JSONConnection jsonConnection = new JSONConnection();
        jsonConnection.execute();

        //Initialize Cardview
        ((LinearLayout)findViewById(R.id.resultLayout)).addView(createCardView("HAllo" + ":", "Ich mag Kekse"));

    }


    //Inner Class for the thread
    private class JSONConnection extends AsyncTask<String, String, String> {

        String json;

        @Override
        protected String doInBackground(String... params) {

            //Get data from Server http://adam.noncd.db.de/api/v1.0/stations/1
            try {
                json = serverConnection.getFromServer("https://iot1jhwest.herokuapp.com/api/backend/railways/get/nodes");
                System.out.println(json);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    //Fuction to add Cardviews based on Information
    public LinearLayout createCardView(String heading, String content){

        //Creating Layout which contains Cardview
        LinearLayout layout = new LinearLayout(this);

        //Creating Cardview which will contain Relative Layout which will then contain TextViews
        CardView cardView = new CardView(this);

        //Creating Layout which contains TextViews
        LinearLayout cardLayout = new LinearLayout(this);

        //Creating TextViews
        TextView headingTV = new TextView(this);
        TextView contentTV = new TextView(this);
        //Creating TextViews's Layout Parameters
        cardLayout.setOrientation(LinearLayout.VERTICAL);

        //Creating Layout Params
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setPadding(20, 20, 20, 20);


        //Setting Layout Params
        layout.setLayoutParams(layoutParams);
        //Setting TextView's Properties
        headingTV.setText(heading);
        headingTV.setTypeface(null, Typeface.BOLD);
        headingTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
        contentTV.setText(content);
        contentTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        //Adding everything to layout
        cardLayout.addView(headingTV);
        cardLayout.addView(contentTV);
        cardView.addView(cardLayout);
        layout.addView(cardView);

        return layout;

    }
}

