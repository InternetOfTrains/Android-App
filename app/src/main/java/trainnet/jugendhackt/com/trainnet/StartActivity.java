package trainnet.jugendhackt.com.trainnet;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import trainnet.jugendhackt.com.trainnet.R;

public class StartActivity extends AppCompatActivity {

    //Initializing calculating button
    Button calculateButton;

    //Initalizing ArrayAdapter
    ArrayAdapter<String> adapter;

    //Initializing Edittext
    AutoCompleteTextView StartLocation;

    //Initializing things for network interactivity
    ConnectivityManager connManager;
    NetworkInfo wifiConnection;

    ServerConnection serverConnection;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //Setting Toolbar instead of Actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        //Connecting Buttons to real IDs
        calculateButton = (Button) findViewById(R.id.buttonSearch);

        //Connecting Edittext
        StartLocation = (AutoCompleteTextView) findViewById(R.id.editTS);

        //Setting up ConnectivityManager
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        //Initialize Items
        serverConnection = new ServerConnection();

        //Create connection to JSON
        JSONConnection jsonConnection = new JSONConnection();
        jsonConnection.execute();


        //Setting OnClick Listeners to Buttons
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //Convert EditTexts to Strings
                String station = StartLocation.getText().toString();

                //Check if all fields are filled in
                if (!TextUtils.isEmpty(station)) {
                    calculateButton.startAnimation(getAnimation("fadeOutAnimation"));


                    //If the FadeOut Animation ends It will take you to the next Activity
                    wifiConnection = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                    if (wifiConnection.isConnected()) {

                        //Direct the user to the Start-Page
                        Intent isConnectedIntent = new Intent(view.getContext(), ResultActivity.class);
                        startActivityForResult(isConnectedIntent, 0);

                    } else if (!wifiConnection.isConnected()) {

                        //Direct the user to the fail-page
                        Intent isConnectedIntent = new Intent(view.getContext(), FailActivity.class);
                        startActivityForResult(isConnectedIntent, 0);

                    }


                }else {

                    //Show Alert-Toast
                    Toast.makeText(getApplicationContext(), "Invalid input!", Toast.LENGTH_SHORT).show();

                    calculateButton.startAnimation(getAnimation("xyz"));


                }
            }

        });

        }

    //Inner Class for the thread
    private class JSONConnection extends AsyncTask<String, String, String> {

        String json;

        @Override
        protected void onPostExecute(String s) {

            //Create
            StartLocation.setAdapter(adapter);

            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {

            //Get data from Server
            try {
                json = serverConnection.getFromServer("https://iot1jhwest.herokuapp.com/api/backend/railways/get/nodes");

                //Parse JSON
                try {
                    JSONObject jsonObject = new JSONObject(json);

                    //Arraylist for Autocomplete
                    List nameList = new ArrayList();

                    //Create Array
                    JSONArray stations = jsonObject.getJSONArray("stations");
                    for (int i = 0; i < stations.length(); i++){

                        //Create a child
                        JSONObject childJSONObject = stations.getJSONObject(i);
                        String stationName = childJSONObject.getString("name");

                        //Add parameters to the list
                        nameList.add(stationName);

                    }

                    ArrayList<String> arrayList = new ArrayList(nameList);

                    //Add Autocomplete to the EditText
                    adapter = new ArrayAdapter<String>(StartActivity.this,android.R.layout.select_dialog_item, arrayList);

                } catch (JSONException e) {
                    System.out.println("Error: " + e);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    public Animation getAnimation(String Name){

        //Implementing Animations
        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadin); //Fades Object in
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fadout); //Fades Object Out
        Animation xyZsahjklhjAnimation = AnimationUtils.loadAnimation(this, R.anim.kopfschuetteln); //lkjdöflkjsdf
        Animation lefttorightAnimation = AnimationUtils.loadAnimation(this, R.anim.left_to_right); //Lets Object go from left to right
        Animation righttoleftAnimation = AnimationUtils.loadAnimation(this, R.anim.right_to_left); //Lets Object go from right to left
        Animation bottomtotopAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top); //Lets Object go from bottom to top
        Animation toptobottomAnimation = AnimationUtils.loadAnimation(this, R.anim.top_to_bottom); //Lets Object go from top to bottom

        //Checking which Animation was requested
        if(Name == "fadeInAnimation"){

            return fadeInAnimation;

        }else if(Name == "fadeOutAnimation"){

            return fadeOutAnimation;

        }else if(Name == "xyz"){

            return xyZsahjklhjAnimation;

        }else if(Name == "lefttorightAnimation"){

            return lefttorightAnimation;

        }else if(Name == "righttoleftAnimation"){

            return righttoleftAnimation;

        }else if(Name == "bottomtotopAnimation"){

            return bottomtotopAnimation;

        }else if(Name == "toptobottomAnimation"){

            return toptobottomAnimation;

        }

        return null;
    }


    //Animation which begins after view was loaded
    @Override
    protected void onResume() {
        super.onResume();

        //Setting Element which will get animated
        CoordinatorLayout activity_main = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        activity_main.startAnimation(getAnimation("bottomtotopAnimation"));

    }
}